sRGB profiles

**Introducing the sRGB_v4_ICC_preference.icc profile**
The sRGB v4 ICC preference profile is a v4 replacement for commonly
used v2 sRGB profiles. It gives better results in workflows that
implement the ICC v4 specification. It is intended to be used in
combination with other ICC v4 profiles.
The advantages of the new profile are:
More pleasing results for most images when combined with any
correctly-constructed v4 output profile using the perceptual
rendering intent.
More consistently correct results among different CMMs using the
ICC-absolute colorimetric rendering intent.
Higher color accuracy using the media-relative colorimetric intent.
A typical use case would be to print sRGB images captured with a
digital still camera. In this case a user could open the image in
Adobe® Photoshop®, assign the sRGB v4 profile as shown in Figure 1.
Adobe® Photoshop® is either a registered trademark or trademark of
Adobe Systems Incorporated in the United States and/or other
countries.
![image](images/assignsrgb.gif)
Figure 1. Assigning the sRGB v4 ICC preference profile. Adobe
product screen shot reprinted with permission from Adobe Systems
Incorporated.
The user would then convert the data to printer specific values
with the sRGB v4 profile as source, the v4 printer profile as
destination, and selecting the perceptual rendering intent, as
shown in Fig 2.
![image](images/convertsrgb.gif)
Figure 2. Conversion from sRGB v4 ICC preference profile to v4
printer profile in Photoshop using the perceptual rendering intent.
Adobe product screen shot reprinted with permission from Adobe
Systems Incorporated.
The three available rendering intents of the sRGB v4 profile should
normally be used as follows:
The ICC-absolute colorimetric rendering intent should be used when
the goal is to maintain the colors of the original on the
reproduction,
The media-relative colorimetric intent should be used when the goal
is to map the source medium white to the destination medium,
The perceptual intent should be used when the goal is to
re-optimize the source colors for the reproduction medium while
maintaining the "look" of the source image
The re-optimization performed by the perceptual rendering intent of
the profile is a result of using the Perceptual Medium Reference
Gamut defined in the ICC v4 specification as an intermediate
rendering target. No perceptual reference medium is defined for v2
profiles.
**Advice for caution**
Ideally the ICC v4 profile should not be combined with ICC v2
profiles. If that is unavoidable, see the intermediate-level ICC
White Paper 26 'Using the sRGB_v4_ICC_preference.icc profile'
for additional information and recommendations.
The sRGB v4 profile will in some cases not produce the same results
as would be obtained using an sRGB v2 profile. Differences will
depend on the particular sRGB v2 profile and rendering intent used.
See the intermediate level white paper for additional information
and recommendations.
**Summary** Overall users can expect to get better and more
consistent results using the sRGB v4 profile versus the sRGB v2
profiles. More details are available in
[White Paper 26](ICC_White_Paper_26_Using_the_V4_sRGB_ICC_profile.pdf).
**sRGB_v4_ICC_preference.icc &nbsp; &nbsp;**
&nbsp;
This sRGB_v4_ICC_preference.icc profile is a Beta version.
Feedback on the performance of the profile (whether positive or
negative) would be greatly welcomed. Please send comments to
[ICC users](mailto:icc_users@lists.color.org) (Subscribe to ICC
users mailing list
[here](http://lists.color.org/mailman/listinfo/icc_users).)

* * * * *

**v2 profiles**
Two ICC version 2 (v2) sRGB profiles are provided below, one with
XYZ black point scaling to zero, and one without. The 'black
scaled' profile has the preferred rendering intent in the header
set to 'perceptual'. and the 'no black scaling' profile has the
preferred rendering intent in the header set to 'relative
colorimetric'.
Both profiles contain the standard linearized Bradford D65 to D50
chromatic adaptation tag (this tag was often not present in older
sRGB profiles), and the media white point tag is set to D50 (as is
required for ICC v4 profiles). This avoids the inappropriate color
casts that older sRGB v2 ICC profiles sometimes produced when the
absolute colorimetric intent was used.
The 'black scaled' profile will work correctly when either Black
Point Compensation (BPC) is on in the CMM/application, or the other
profile transform to be used for the conversion is also black
scaled. Black scaled profiles are needed when BPC is desired for
the conversion but the CMM to be used does not support BPC. Most v2
sRGB profiles in current use are black scaled because they are
intended to be used as the source profile in conversions where the
destination profile perceptual intent will be used (v2 perceptual
transforms typically include black scaling).
The 'no black scaling' profile produces sRGB colorimetry in the PCS
that is more accurate to what a viewer of a sRGB calibrated display
will observe in the reference sRGB viewing conditions. This is the
case when the display has been calibrated to include the
viewer-observed black level such as one might achieve with a
non-contact monitor calibrator or other means to include ambient
effects. It should be used in situations where greater colorimetric
accuracy is desired, e.g. as the destination profile for preview on
calibrated sRGB displays where the viewer-observed black has been
included. However it should be noted that most current LCD
displays, even those that use the sRGB primaries and gamma, have a
higher white point luminance and dynamic range than the sRGB
reference display. To produce accurate preview colorimetry on these
displays they should be accurately profiled, instead of using an
sRGB profile.
The 'no black scaling' profile can be used with BPC on or off in
the CMM. If it is used with BPC on the results should be the same
as when using the 'black scaled' profile. If it is used with BPC
off it will produce un-black-scaled media-relative colorimetry: if
the source encoding has tones darker than the destination encoding
black point they will be clipped, and if the source black point is
higher than the destination black point the source black point will
be accurately reproduced.
Note that these profiles were previously named
'sRGB_IEC61966-2-1_noBPC.icc' 'sRGB_IEC61966-2-1_withBPC.icc'
to indicate whether black scaling had been applied in the profile.
They have been renamed to reduce ambiguity over their use with
black point compensation.
**Terms of use**
To anyone who acknowledges that the files
"sRGB_IEC61966-2-1_no_black_scaling.icc" and
"sRGB_IEC61966-2-1_black scaled.icc" are provided "AS IS" WITH NO
EXPRESS OR IMPLIED WARRANTY, permission to use, copy and distribute
these file for any purpose is hereby granted without fee, provided
that the files are not changed including the ICC copyright notice
tag, and that the name of ICC shall not be used in advertising or
publicity pertaining to distribution of the software without
specific, written prior permission. ICC makes no representations
about the suitability of this software for any purpose.
